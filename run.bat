@echo off
cd python
python demo.py -image ../source.png -weights train_result/depth -model depth -output ../demo_data/python-depth.npy
python demo.py -image ../source.png -weights ../weights/semantics -model semantic -output ../demo_data/python-semantic.npy
python demo.py -image ../source.png -weights train_result/map -model map -output ../demo_data/python-map.npy
cd ..
python visualize.py python demo_data/python-depth.npy demo_data/python-depth.png depth
python visualize.py python demo_data/python-map.npy demo_data/python-map.png map
python visualize.py python demo_data/python-semantic.npy demo_data/python-semantic.png semantic
