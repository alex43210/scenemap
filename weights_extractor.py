import torchfile
import sys
import os
import numpy


def main(args):
    def get_weights(obj):
        weights = []
        if b"weights" in obj._obj:
            weights.append(obj._obj[b"weights"])
        if b"bias" in obj._obj:
            weights.append(obj._obj[b"bias"])
        if b"modules" in obj._obj:
            for module in obj._obj[b"modules"]:
                weights += get_weights(module)
        return weights

    if len(args) != 3:
        print("Need 2 arguments: path to torch model & path to directory to store weights")
    model = torchfile.load(args[1])
    weights = get_weights(model)
    for i, weight in enumerate(weights):
        weight_path = os.path.join(args[2], "{0}.npy".format(i))
        numpy.save(weight_path, weight)


if __name__ == "__main__":
    main(sys.argv)