require 'torch'
local loader = require("loader")
local npy4th = require 'npy4th'


function main(params)
    local gpu = tonumber(params["gpu"])
    local backend_module
    local use_cuda
    backend_module, use_cuda = setupGpu(gpu)
    local test = params["test"]
    if test == "rgb" then
        rgbTest(params)
    elseif test == "depth" then
        depthTest(params)
    elseif test == "semantic" then
        semanticTest(params)
    elseif test == "map" then
        mapTest(params)
    elseif test == "weights" then
        weightLoadingTest(params)
    elseif test == "depth_cnn" then
        depthCnnTest(params, backend_module, use_cuda)
    elseif test == "map_cnn" then
        mapCnnTest(params, backend_module, use_cuda)
    elseif test == "semantic_cnn" then
        semanticCnnTest(params, backend_module, use_cuda)
    else
        print("Invalid test")
        os.exit(1)
    end
end


function setupGpu(gpu)
    local backend
    if gpu >= 0 then
        cutorch = require "cutorch"
        require "cunn"
        cutorch.setDevice(gpu)
        backend = "cunn"
    else
        require "nn"
        backend = "nn"
    end
    return require(backend), false
end


function output(path, data)
    torch.save(path, data)
end


function rgbTest(params)
    print("Loading RGB data from " .. params["rgb"])
    local data = loader.RGB(params["rgb"], 1000)
    output(params["rgb_output"], data)
    print("Saved RGB data")
end


function depthTest(params)
    print("Loading depth data from " .. params["depth"])
    local data = loader.depth(params["depth"], 1000)
    output(params["depth_output"], data)
    print("Saved depth data")
end


function semanticTest(params)
    print("Loading semantic data from " .. params["semantic"])
    local data = loader.semantic(params["semantic"], 1000, 1)
    output(params["semantic_output"], data)
    print("Saved semantic data")
end


function mapTest(params)
    print("Loading map data from " .. params["map"])
    local data = loader.map(params["map"], 1000, 1)
    output(params["map_output"], data)
    print("Saved map data")
end


function weightLoadingTest(params)
    local path = params["weights"] .. "/" .. params["weight"] .. ".npy"
    print("Loading weights data " .. path)
    local data = npy4th.loadnpy(path)
    output(params["weights_output"], data)
    print("Saved weights data")
end


function calculateModelOnBatches(model_func, data, batch_size)
    local size = data:size()
    local item_count = size[1]
    local batch_count = math.ceil(item_count / batch_size)
    local result = nil
    for i=1, batch_count do
        local model = model_func()
        print("Batch " .. tostring(i) .. " of " .. tostring(batch_count))
        local batch_data = data:narrow(1, 1 + (i - 1) * batch_size, batch_size)
        local batch_result = model:forward(batch_data)
        if result == nil then
            result = batch_result
        else
            result = torch.cat(result, batch_result, 1)
        end
    end
    return result
end


function depthCnnTest(params, backend_module, use_cuda)
    print("Loading weights")
    local weights_directory = params["weights"]
    local images_path = params["rgb"]
    local weights = {}
    for i=1,56,1 do
        local path = weights_directory .. "/" .. tostring(i - 1) .. ".npy"
        weights[i] = npy4th.loadnpy(path)
    end
    print("Instantiate depth model")
    local depth_factory = require("models/depth")
    print("Load RGB data")
    local data = loader.RGB(images_path, 8)
    print("Calculating output")
    local result = calculateModelOnBatches(
        function() return depth_factory(backend_module, use_cuda, weights) end,
        data,
        8)
    output(params["cnn_output"], result)
    print("Saved depth CNN output")
end


function mapCnnTest(params, backend_module, use_cuda)
    print("Loading weights")
    local weights_directory = params["weights"]
    local images_path = params["rgb"]
    local weights = {}
    for i=1,36,1 do
        local path = weights_directory .. "/" .. tostring(i - 1) .. ".npy"
        weights[i] = npy4th.loadnpy(path)
    end
    print("Instantiate map model")
    local map_factory = require("models/map")
    print("Load RGB data")
    local data = loader.RGB(images_path, 8)
    print("Calculating output")
    local result = calculateModelOnBatches(
        function() return map_factory(backend_module, use_cuda, 4, 16, weights) end,
        data,
        8)
    output(params["cnn_output"], result)
    print("Saved depth CNN output")
end


function semanticCnnTest(params, backend_module, use_cuda)
    print("Loading weights")
    local weights_directory = params["weights"]
    local images_path = params["rgb"]
    local weights = {}
    for i=1,56,1 do
        local path = weights_directory .. "/" .. tostring(i - 1) .. ".npy"
        weights[i] = npy4th.loadnpy(path)
    end
    print("Instantiate semantic model")
    local semantics_factory = require("models/semantics")
    print("Load RGB data")
    local data = loader.RGB(images_path, 8)
    print("Calculating output")
    local result = calculateModelOnBatches(
        function() return semantics_factory(backend_module, use_cuda, 4, weights) end,
        data,
        8)
    output(params["cnn_output"], result)
    print("Saved semantics CNN output")
end


local cmd = torch.CmdLine()
cmd:option('-gpu', '0', 'Zero-indexed ID of the GPU to use; for CPU mode set -gpu = -1')
cmd:option('-rgb', '', 'RGB data path')
cmd:option('-rgb_output', '', 'RGB output path')
cmd:option('-depth', '', 'Depth data path')
cmd:option('-depth_output', '', 'Depth output path')
cmd:option('-semantic', '', 'Semantic data path')
cmd:option('-semantic_output', '', 'Semantic output path')
cmd:option('-map', '', 'Map data path')
cmd:option('-map_output', '', 'Map output path')
cmd:option('-weights', '', 'Weight data path')
cmd:option('-weight', '', 'Weight index')
cmd:option('-weights_output', '', 'Weight output path')
cmd:option('-cnn_output', '', 'CNN output path')
cmd:option('-test', '', 'RGB reader test')
local params = cmd:parse(arg)
main(params)