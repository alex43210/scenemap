require 'util/fastwrite'
local torch = require("torch")


function RGB(path, buffer_size)
    local buffer = torch.Tensor(buffer_size, 224, 224, 3):byte()
    fastReadByte(path, buffer)
    return buffer:transpose(2, 4):transpose(3, 4):float():div(255)
end


function depth(path, buffer_size)
    local tmp_target = torch.Tensor(buffer_size, 224, 224, 3):float()
    fastReadFloat(path, tmp_target)
    return tmp_target:transpose(2, 4):transpose(3, 4):narrow(2, 1, 1)
end


function semantic(path, buffer_size, n_classes)
    local target = torch.Tensor(buffer_size, 224, 224):float()
    local tmp_target = torch.Tensor(buffer_size, n_classes + 2, 224, 224):byte()
    local _
    fastReadByte(path, tmp_target)
    for i=1, buffer_size do
        _, target[i] = torch.max(tmp_target[i], 1)
    end
    return target
end


function map(path, buffer_size, n_classes)
    local target = torch.Tensor(buffer_size, n_classes + 2, 16, 16):int()
    fastReadInt(path, target)
    return target:narrow(2, 3, n_classes):float()
end


return {
    ["RGB"] = RGB,
    ["depth"] = depth,
    ["semantic"] = semantic,
    ["map"] = map,
}