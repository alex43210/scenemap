local nn = require("nn")

function create_model(backend_module, use_cuda, n_classes, weights)
    local Convolution = backend_module.SpatialConvolution
    local ReLU = backend_module.ReLU
    local Max = nn.SpatialMaxPooling
    local UnMax = nn.SpatialMaxUnpooling
    local SBatchNorm = backend_module.SpatialBatchNormalization
    local weights_counter = 1

    local function set_layer_weights(layer)
        if weights ~= nil then
            layer.weight = weights[weights_counter]
            layer.bias = weights[weights_counter + 1]
            weights_counter = weights_counter + 2
        end
        return layer
    end

    local function add_layer(n, pool, model, no_relu)
        local n_input_plane = i_channels
        i_channels = n

        model:add(set_layer_weights(Convolution(n_input_plane, n, 3, 3, 1, 1, 1, 1)))
        model:add(set_layer_weights(SBatchNorm(n, 1e-3)))

        if not no_relu then
            model:add(ReLU(true))
        end

        local smp
        if pool then
            smp = Max(2, 2)
            model:add(smp)
        else
            smp = nil
        end

        return smp
    end

    function add_unpool(smp, model)
        model:add(UnMax(smp))
    end

    model = nn.Sequential()

    i_channels = 3
    smp1 = add_layer(64,  true, model)
    smp2 = add_layer(128, true, model)
    add_layer(256, false, model)
    smp3 = add_layer(256, true,  model)
    add_layer(512, false, model)
    smp4 = add_layer(512, true,  model)
    smp5 = add_layer(512, true,  model)
    add_unpool(smp5, model)
    add_layer(512, false, model)
    add_unpool(smp4, model)
    add_layer(512, false, model)
    add_layer(256, false, model)
    add_unpool(smp3, model)
    add_layer(256, false, model)
    add_layer(128, false, model)
    add_unpool(smp2, model)
    add_layer(64, false, model)
    add_unpool(smp1, model)

    add_layer(n_classes + 2, false, model, true)

    ----------------
    -- MODEL INIT --
    ----------------
    if weights == nil then
        model = require('util/weight-init')(model, 'xavier')
    end

    if use_cuda then
        return model:cuda()
    else
        return model:float()
    end
end

return create_model
