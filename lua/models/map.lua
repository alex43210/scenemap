local nn = require("nn")

-----------------
-- MODEL SETUP --
-----------------
function create_model(backend_module, use_cuda, n_classes, grid_size, weights)
    local Convolution = backend_module.SpatialConvolution
    local ReLU = backend_module.ReLU
    local Sigmoid = backend_module.Sigmoid
    local Max = nn.SpatialMaxPooling
    local SBatchNorm = backend_module.SpatialBatchNormalization
    local BatchNorm = nn.BatchNormalization
    local View = nn.View
    local Linear = nn.Linear
    local weights_counter = 1

    local function set_layer_weights(layer)
        if weights ~= nil then
            layer.weight = weights[weights_counter]
            layer.bias = weights[weights_counter + 1]
            weights_counter = weights_counter + 2
        end
        return layer
    end

    local function layer(n, pool, model)
        local n_input_plane = i_channels
        i_channels = n

        model:add(set_layer_weights(Convolution(n_input_plane, n, 3, 3, 1, 1, 1, 1)))
        model:add(set_layer_weights(SBatchNorm(n, 1e-3)))
        model:add(ReLU(true))
        if pool then
            model:add(Max(2, 2))
        end
    end

    local function fc_layer(n, model)
        local n_input_plane = i_channels
        i_channels = n

        model:add(set_layer_weights(Linear(n_input_plane, n)))
        model:add(set_layer_weights(BatchNorm(n, 1e-3)))
    end

    model = nn.Sequential()
    i_channels = 3
    layer(64, true, model)
    layer(128, true, model)
    layer(256, false, model)
    layer(256, true, model)
    layer(512, false, model)
    layer(512, true, model)
    layer(512, true, model)
    model:add(set_layer_weights(Convolution(1024, 1024, 7, 7, 1, 1, 0, 0)))
    model:add(set_layer_weights(SBatchNorm(1024, 1e-3)))
    model:add(View(1024))

    local n_out_pixels = n_classes * grid_size * grid_size
    i_channels = 1024
    fc_layer(n_out_pixels, model)

    model:add(nn.Reshape(n_classes, grid_size, grid_size))
    model:add(Sigmoid())

    ----------------
    -- MODEL INIT --
    ----------------
    if weights == nil then
        model = require('util/weight-init')(model, 'xavier')
    end

    if use_cuda then
        return model:cuda()
    else
        return model:float()
    end
end

return create_model
