require("torch")
require("image")
local npy4th = require 'npy4th'


function main(params)
    local backend
    local use_cuda
    backend, use_cuda = setupGpu(tonumber(params["gpu"]))
    local img = image.scale(image.load(params["image"], 3, 'byte'), 224, 224):float():div(255):reshape(1, 3, 224, 224)
    local model
    if params["model"] == "depth" then
        model = initDepthModel(backend, use_cuda, params["weights"])
    elseif params["model"] == "map" then
        model = initMapModel(backend, use_cuda, params["weights"])
    elseif params["model"] == "semantic" then
        model = initSemanticModel(backend, use_cuda, params["weights"])
    else
        os.exit(1)
    end
    local result = model:forward(torch.cat(img, img, 1)):narrow(1, 1, 1) -- had problem with batchnormalization on 1 image.
    torch.save(params["output"], result)
end


function setupGpu(gpu)
    local backend
    if gpu >= 0 then
        cutorch = require "cutorch"
        require "cunn"
        cutorch.setDevice(gpu)
        backend = "cunn"
    else
        require "nn"
        backend = "nn"
    end
    return require(backend), false
end


function loadWeights(directory, count)
    local weights = {}
    for i=1,count,1 do
        local path = directory .. "/" .. tostring(i - 1) .. ".npy"
        weights[i] = npy4th.loadnpy(path)
    end
    return weights
end


function initDepthModel(backend_module, use_cuda, weights_directory)
    local weights = loadWeights(weights_directory, 56)
    local factory = require("models/depth")
    return factory(backend_module, use_cuda, weights)
end


function initMapModel(backend_module, use_cuda, weights_directory)
    local weights = loadWeights(weights_directory, 36)
    local factory = require("models/map")
    return factory(backend_module, use_cuda, 4, 16, weights)
end


function initSemanticModel(backend_module, use_cuda, weights_directory)
    local weights = loadWeights(weights_directory, 56)
    local factory = require("models/semantics")
    return factory(backend_module, use_cuda, 4, weights)
end


local cmd = torch.CmdLine()
cmd:option('-gpu', '0', 'Zero-indexed ID of the GPU to use; for CPU mode set -gpu = -1')
cmd:option('-image', '0', 'Zero-indexed ID of the GPU to use; for CPU mode set -gpu = -1')
cmd:option('-weights', '', 'Weight data path')
cmd:option('-model', '', 'Model name (depth/map/semantic)')
cmd:option('-output', '', 'Output path')
local params = cmd:parse(arg)
main(params)