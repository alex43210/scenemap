#!/bin/bash
export THEANO_FLAGS="device=gpu0"

if [ ! -d demo_data ]; then
    mkdir demo_data
fi
rm -rf demo_data/*

cd lua
time luajit demo.lua -gpu -1 -image ../demo-image.png -weights "$1" -model depth -output ../demo_data/lua-depth.t7
time luajit demo.lua -gpu -1 -image ../demo-image.png -weights "$2" -model map -output ../demo_data/lua-map.t7
time luajit demo.lua -gpu -1 -image ../demo-image.png -weights "$3" -model semantic -output ../demo_data/lua-semantic.t7

cd ../python
time python3 demo.py -image ../demo-image.png -weights "$1" -model depth -output ../demo_data/python-depth.npy
time python3 demo.py -image ../demo-image.png -weights "$2" -model map -output ../demo_data/python-map.npy
time python3 demo.py -image ../demo-image.png -weights "$3" -model semantic -output ../demo_data/python-semantic.npy
cd ..

python3 visualize.py python demo_data/python-depth.npy demo_data/python-depth.png depth
python3 visualize.py torch demo_data/lua-depth.t7 demo_data/lua-depth.png depth
python3 visualize.py python demo_data/python-map.npy demo_data/python-map.png map
python3 visualize.py torch demo_data/lua-map.t7 demo_data/lua-map.png map
python3 visualize.py torch demo_data/lua-semantic.t7 demo_data/lua-semantic.png semantic
python3 visualize.py python demo_data/python-semantic.npy demo_data/python-semantic.png semantic
