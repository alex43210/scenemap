import sys
import numpy
from models import Depth, Map, Semantic
from loader import read_rgb, read_depth, read_map, read_semantic
import argparse
import os


def main(args):
    assert args['model'] in ['depth', 'map', 'semantic']
    input = read_rgb(args['rgb'], int(args["process_image_count"]))
    if args['model'] == 'depth':
        model = Depth(batch_size=int(args['batch_size']))
        data = read_depth(args["output"], int(args["process_image_count"]))
    elif args['model'] == 'semantic':
        model = Semantic(int(args["class_count"]), batch_size=int(args['batch_size']))
        semantic_data = read_semantic(args["output"], int(args["process_image_count"]), int(args["class_count"]))
        data = numpy.zeros([semantic_data.shape[0], int(args["class_count"]) + 2, semantic_data.shape[1], semantic_data.shape[2]])
        for i in range(int(args["process_image_count"])):
            for x in range(224):
                for y in range(224):
                    top_class = semantic_data[i, x, y] - 1
                    data[i, top_class, x, y] = 1
    elif args['model'] == 'map':
        model = Map(int(args["class_count"]), int(args["grid_size"]), batch_size=int(args['batch_size']))
        data = read_map(args["output"], int(args["process_image_count"]), int(args["class_count"]))
    if "weights" in args and args["weights"]:
        weight_counts = {
            "depth": 56,
            "map": 36,
            "semantic": 56
        }
        weight_count = weight_counts[args["model"]]
        weights = []
        for i in range(0, weight_count):
            path = args["weights"] + "/" + str(i) + ".npy"
            weight = numpy.load(path)
            weights.append(weight)
        model.set_weights(weights)
    model.fit(input, data, int(args["epochs"]), verbose=True)
    weights = model.get_weights()
    for i in range(0, len(weights)):
        path = os.path.join(args["result_path"], "{0}.npy".format(i))
        numpy.save(path, weights[i])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-model")
    parser.add_argument("-rgb")
    parser.add_argument("--weights")
    parser.add_argument("-result_path")
    parser.add_argument("-output")
    parser.add_argument("-epochs")
    parser.add_argument("-batch_size")
    parser.add_argument("-process_image_count")
    parser.add_argument("--class_count")
    parser.add_argument("--grid_size")
    args = vars(parser.parse_args())
    main(args)