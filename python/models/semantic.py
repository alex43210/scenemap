from keras.layers import InputLayer, Activation
from .layers import Conv2D, BatchNorm4D, Pooling2D, Unpooling2D
from .model import Model


class Semantic(Model):
    """
    Semantic estimator model.
    Input have shape: [imageCount, colorChanelCount, width, height] where:
    - imageCount - count of images for train/prediction
    - colorChanelCount = 3
    - width = 224
    - height = 224
    Output have shape
    [classCount, width, height]
    and each item [classIndex, x, y] mean a confidence about location of object of given class into point x, y
    """

    def __init__(self, class_count, weights=None, batch_size=10):
        self.class_count = class_count
        super(Semantic, self).__init__(weights, batch_size)

    def _build_model(self):
        self.model.add(InputLayer((3, 224, 224)))
        pool1 = self._add_layer(64, True, True)
        pool2 = self._add_layer(128, True, True)
        self._add_layer(256, False, True)
        pool3 = self._add_layer(256, True, True)
        self._add_layer(512, False, True)
        pool4 = self._add_layer(512, True, True)
        pool5 = self._add_layer(512, True, True)
        self.model.add(Unpooling2D(pool5))
        self._add_layer(512, False, True)
        self.model.add(Unpooling2D(pool4))
        self._add_layer(512, False, True)
        self._add_layer(256, False, True)
        self.model.add(Unpooling2D(pool3))
        self._add_layer(256, False, True)
        self._add_layer(128, False, True)
        self.model.add(Unpooling2D(pool2))
        self._add_layer(64, False, True)
        self.model.add(Unpooling2D(pool1))
        self._add_layer(self.class_count + 2, False, False)

    def _add_layer(self, size, pool, relu):
        self.model.add(Conv2D(size, 3, 3, (1, 1,)))
        self.model.add(BatchNorm4D(1e-3))
        if relu:
            self.model.add(Activation('relu'))
        if pool:
            pooling = Pooling2D()
            self.model.add(pooling)
            return pooling
