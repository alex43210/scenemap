from keras.regularizers import L1L2Regularizer
from keras.layers import InputLayer, Activation, Flatten, Reshape
from .layers import Conv2D, BatchNorm4D, BatchNorm2D, Pooling2D, Unpooling2D, Linear
from .model import Model


class Map(Model):
    """
    Map estimator model.
    Input have shape: [imageCount, colorChanelCount, width, height] where:
    - imageCount - count of images for train/prediction
    - colorChanelCount = 3
    - width = 224
    - height = 224
    Output have shape
    [imageCount, classCount, gridSize, gridSize]
    and each item [classIndex, x, y] mean a confidence about location of object of given class into point x, y
    """
    def __init__(self, class_count, grid_size, weights=None, batch_size=10):
        self.class_count = class_count
        self.grid_size = grid_size
        super(Map, self).__init__(weights, batch_size)

    def _build_model(self):
        self.model.add(InputLayer((3, 224, 224)))
        self._add_layer(64, True)
        self._add_layer(128, True)
        self._add_layer(256, False)
        self._add_layer(256, True)
        self._add_layer(512, False)
        self._add_layer(512, True)
        self._add_layer(512, True)
        self.model.add(Conv2D(1024, 7, 7, (1, 1,), 'valid'))
        self.model.add(BatchNorm4D(1e-3))
        self.model.add(Flatten())
        n_out_pixels = self.class_count * self.grid_size * self.grid_size
        self.model.add(Linear(n_out_pixels))
        self.model.add(BatchNorm2D(1e-3,
                                   W_regularizer=L1L2Regularizer(1e-8, 0)))
        self.model.add(Reshape([self.class_count, self.grid_size, self.grid_size]))
        self.model.add(Activation('sigmoid'))

    def _add_layer(self, size, pool):
        self.model.add(Conv2D(size, 3, 3, (1, 1,)))
        self.model.add(BatchNorm4D(1e-3))
        self.model.add(Activation('relu'))
        if pool:
            self.model.add(Pooling2D())
