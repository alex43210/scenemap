from .depth import Depth
from .map import Map
from .semantic import Semantic
from .model import Model