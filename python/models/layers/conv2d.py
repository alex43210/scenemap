from keras.layers import Layer
from keras.engine import InputSpec
from keras import backend as K
from keras import initializations
from keras.utils.np_utils import conv_output_length
from theano.tensor.nnet import conv2d


class Conv2D(Layer):
    def __init__(self, nb_filter, nb_row, nb_col, subsample, border_mode='same', **kwargs):
        assert K.backend() == 'theano'
        self.init = initializations.get('glorot_uniform', dim_ordering='th')
        self.built = False
        self.input_spec = [InputSpec(ndim=4)]
        self.nb_filter = nb_filter
        self.nb_row = nb_row
        self.nb_col = nb_col
        self.subsample = subsample
        self.border_mode = border_mode
        super(Conv2D, self).__init__(**kwargs)

    def build(self, input_shape):
        stack_size = input_shape[1]
        self.W_shape = (self.nb_filter, stack_size, self.nb_row, self.nb_col)
        self.W = self.add_weight(self.W_shape,
                                 initializer=self.init,
                                 name='{}_W'.format(self.name))
        self.b = self.add_weight((self.nb_filter,),
                                 initializer='zero',
                                 name='{}_b'.format(self.name))
        self.built = True

    def get_output_shape_for(self, input_shape):
        rows = input_shape[2]
        cols = input_shape[3]
        rows = conv_output_length(rows, self.nb_row,
                                  self.border_mode, self.subsample[0])
        cols = conv_output_length(cols, self.nb_col,
                                  self.border_mode, self.subsample[1])
        return (input_shape[0], self.nb_filter, rows, cols)

    def call(self, x, mask=None):
        if self.border_mode == 'same':
            border_mode = 'half'
        else:
            border_mode = self.border_mode
        output = conv2d(x,
                        self.W,
                        subsample=self.subsample,
                        border_mode=border_mode,
                        filter_shape=self.W_shape,
                        filter_flip=False) + \
                 K.reshape(self.b, (1, self.nb_filter, 1, 1))
        return output