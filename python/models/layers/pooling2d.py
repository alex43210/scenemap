from keras.layers import Layer
from theano import tensor as T
from keras import backend as K


class Pooling2D(Layer):
    def __init__(self, **kwargs):
        assert K.backend() == 'theano'
        super(Pooling2D, self).__init__(**kwargs)
        self.built = False

    def build(self, input_shape):
        self.indices = None
        self.built = True

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], input_shape[1], int(input_shape[2] / 2), int(input_shape[3] / 2),)

    def call(self, x, mask=None):
        reshaped = x.reshape([
            x.shape[0], x.shape[1], x.shape[2] // 2, 2, x.shape[3] // 2, 2
        ])
        max_values, self.indices = T.max_and_argmax(reshaped, (3, 5,))
        return max_values