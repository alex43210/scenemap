from keras.layers import Layer
from theano import tensor as T
from keras import backend as K


class Unpooling2D(Layer):
    def __init__(self, pooling, **kwargs):
        assert K.backend() == 'theano'
        self.pooling = pooling
        super(Unpooling2D, self).__init__(**kwargs)
        self.built = False

    def build(self, input_shape):
        self.built = True

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], input_shape[1], input_shape[2] * 2, input_shape[3] * 2,)

    def call(self, x, mask=None):
        indices = self.pooling.indices
        tmp_shape = [x.shape[0], x.shape[1], x.shape[2], 2, x.shape[3], 2]
        # Resize image
        resized = x.repeat(2, 2).repeat(2, 3)
        pooled_reshaped = resized.reshape(tmp_shape)
        # Resize indices
        indices_repeaten = indices.repeat(2, 2).repeat(2, 3).reshape(tmp_shape)
        # Calculate output
        result = T.set_subtensor(pooled_reshaped[:, :, :, 0, :, 0],
                                 pooled_reshaped[:, :, :, 0, :, 0] * T.eq(indices_repeaten[:, :, :, 0, :, 0], 0))
        result = T.set_subtensor(result[:, :, :, 0, :, 1],
                                 pooled_reshaped[:, :, :, 0, :, 1] * T.eq(indices_repeaten[:, :, :, 0, :, 1], 1))
        result = T.set_subtensor(result[:, :, :, 1, :, 0],
                                 pooled_reshaped[:, :, :, 1, :, 0] * T.eq(indices_repeaten[:, :, :, 1, :, 0], 2))
        result = T.set_subtensor(result[:, :, :, 1, :, 1],
                                 pooled_reshaped[:, :, :, 1, :, 1] * T.eq(indices_repeaten[:, :, :, 1, :, 1], 3))
        result_shape = [x.shape[0], x.shape[1], x.shape[2] * 2, x.shape[3] * 2]
        return result.reshape(result_shape)