from keras.layers import Layer
from keras.engine import InputSpec
from keras import backend as K
from keras import initializations
from theano import tensor as T


class BaseBatchNorm(Layer):
    def __init__(self, epsilon, ndim, axes, W_regularizer=None, b_regularizer=None, **kwargs):
        assert K.backend() == 'theano'
        self.init = initializations.get('glorot_uniform', dim_ordering='th')
        self.built = False
        self.input_spec = [InputSpec(ndim=ndim)]
        self.epsilon = epsilon
        self.ndim = ndim
        self.axes = axes
        self.W_regularizer = W_regularizer
        self.b_regularizer = b_regularizer
        super(BaseBatchNorm, self).__init__(**kwargs)

    def build(self, input_shape):
        input_size = input_shape[1]
        shape = [input_size]
        self.W = self.add_weight(shape,
                                 initializer=self.init,
                                 regularizer=self.W_regularizer,
                                 name='{}_W'.format(self.name))
        self.b = self.add_weight(shape,
                                 initializer='zero',
                                 regularizer=self.b_regularizer,
                                 name='{}_b'.format(self.name))
        self.built = True

    def get_output_shape_for(self, input_shape):
        return input_shape

    def call(self, x, mask=None):
        mean = x.mean(self.axes)
        inv_std = T.inv(T.sqrt(x.var(self.axes) + self.epsilon))
        param_axes = iter(range(self.ndim - len(self.axes)))
        pattern = ['x' if input_axis in self.axes
                   else next(param_axes)
                   for input_axis in range(self.ndim)]
        beta_shuffled = self.b.dimshuffle(pattern)
        gamma_shuffled = self.W.dimshuffle(pattern)
        mean = mean.dimshuffle(pattern)
        inv_std = inv_std.dimshuffle(pattern)
        return (x - mean) * (gamma_shuffled * inv_std) + beta_shuffled


class BatchNorm4D(BaseBatchNorm):
    def __init__(self, epsilon, **kwargs):
        super(BatchNorm4D, self).__init__(epsilon, 4, [0, 2, 3], **kwargs)


class BatchNorm2D(BaseBatchNorm):
    def __init__(self, epsilon, **kwargs):
        super(BatchNorm2D, self).__init__(epsilon, 2, [0], **kwargs)