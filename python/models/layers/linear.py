from keras.layers import Layer
from keras.engine import InputSpec
from keras import backend as K
from theano import tensor as T
from keras import initializations
import math


class Linear(Layer):
    def __init__(self, n_out_pixels, **kwargs):
        assert K.backend() == 'theano'
        self.n_out_pixels = n_out_pixels
        self.init = initializations.get('glorot_uniform', dim_ordering='th')
        self.built = False
        self.input_spec = [InputSpec(ndim=2)]
        super(Linear, self).__init__(**kwargs)

    def build(self, input_shape):
        self.repeat_count = math.ceil(self.n_out_pixels / input_shape[1])
        stack_size = input_shape[1]
        self.W_shape = (stack_size, stack_size,)
        self.W = self.add_weight(self.W_shape,
                                 initializer=self.init,
                                 name='{}_W'.format(self.name))
        self.b = self.add_weight((stack_size,),
                                 initializer='zero',
                                 name='{}_b'.format(self.name))
        self.built = True

    def get_output_shape_for(self, input_shape):
        return input_shape

    def call(self, x, mask=None):
        repeated = x.repeat(self.repeat_count, axis=0)
        repeated_flat = repeated.reshape([x.shape[0], self.repeat_count * x.shape[1]])
        resized = repeated_flat[:, 0:self.n_out_pixels]
        return K.map_fn(lambda row: K.dot(self.W, row) + self.b,
                        resized)