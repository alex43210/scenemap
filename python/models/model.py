from keras.models import Sequential
from keras.optimizers import Adam


class Model:
    def __init__(self, weights=None, batch_size=10):
        """
        Base model class
        :param weights: weights or None
        :type weights: list[numpy.ndarray]|NoneType
        :param batch_size: batch size
        :type batch_size: int
        """
        self.batch_size = batch_size
        self.model = Sequential()
        self.optimizer = Adam(lr=0.01)  # Use stochastic gradient descent as default optimizer
        self.metrics = "mse"   # Use MSE as default loss metric
        self._build_model()
        self.model.compile(self.optimizer, self.metrics)
        self.set_weights(weights)

    def _build_model(self):
        raise NotImplementedError("Model._build_model() is abstract")

    def set_weights(self, weights):
        """
        Set model weights
        :param weights: weights or None to reset
        :type weights: list[numpy.ndarray]|NoneType
        """
        if weights is None:
            self.model.reset_states()
        else:
            self.model.set_weights(weights)

    def get_weights(self):
        """
        Get weights
        :return: weights
        :rtype: list[numpy.ndarray]
        """
        return self.model.get_weights()

    def predict(self, features, verbose=False):
        """
        Make prediction
        :param features: input data
        :type features: numpy.ndarray
        :param verbose: show progress?
        :type verbose: bool
        :return: result
        :rtype: numpy.ndarray
        """
        return self.model.predict(features, batch_size=self.batch_size, verbose=verbose)

    def fit(self, x, y, epochs, verbose=False, callbacks=[]):
        """
        Train model
        :param x: input examples
        :type x: numpy.ndarray
        :param y: output examples
        :type y: numpy.ndarray
        :param epochs: epoch count
        :type epochs: int
        :param verbose: show progress?
        :type verbose: bool
        :param callbacks: train callbacks list (e.g. [keras.callbacks.EarlyStopping(...)])
        """
        self.model.fit(x, y, nb_epoch=epochs, batch_size=self.batch_size, verbose=verbose, callbacks=callbacks)
