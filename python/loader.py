import numpy


def read_rgb(path, image_count):
    """
    Read RGB data
    :param path: binary file path. \
        File must contains image as bytes. \
        It can be presented as "flatten" array with next structure `uint8 [count, 224, 224, 3]`
    :type path: str
    :param image_count: count of image to read
    :type image_count: int
    :return: images as array with shape [image_count, 3, 224, 224] where each element - in 0..1
    :rtype: numpy.ndarray
    """
    byte_count = image_count * 224 * 224 * 3
    with open(path, "rb") as source:
        images_bytes = source.read(byte_count)
    images_bytes_np = numpy.fromstring(images_bytes, numpy.uint8)
    images = images_bytes_np.reshape([image_count, 224, 224, 3])
    return numpy.transpose(images, [0, 3, 1, 2]).astype(numpy.float32) / 255.0


def read_depth(path, count):
    """
    Read depth data
    :param path: binary file path. Contains depth data as float32. \
        It can be presented as "flatten" array with next structure `float32 [count, 224, 224, 3]`.
        Will use first chanell.
    :type path: str
    :param count: record count
    :type count: int
    :return: depth data as array with shape [count, 1, 224, 224]
    """
    byte_count = count * 224 * 224 * 3 * 4
    with open(path, "rb") as source:
        depth_bytes = source.read(byte_count)
    depth_flat = numpy.fromstring(depth_bytes, numpy.float32)
    depth = depth_flat.reshape([count, 224, 224, 3])
    transposed_depth = numpy.transpose(depth, [0, 3, 1, 2])
    return transposed_depth[:, 0, :, :].reshape([count, 1, 224, 224])


def read_semantic(path, count, n_classes):
    """
    Read semantic data
    :param path: path to binary data. \
        It can be presented as "flatten" array with next structure `uint8 [count, n_classes + 2, 224, 224]`.
    :param path: str
    :param count: record count
    :param count: int
    :param n_classes: class count
    :type n_classes: int
    :return: semantic data as array with view [count, width, height, class_index]
    """
    byte_count = count * (n_classes + 2) * 224 * 224
    with open(path, "rb") as source:
        semantic_bytes = source.read(byte_count)
    semantic_flat = numpy.fromstring(semantic_bytes, numpy.uint8)
    semantic_tmp = semantic_flat.reshape([count, n_classes + 2, 224, 224])
    semantic = numpy.argmax(semantic_tmp, axis=1) + 1
    return semantic


def read_map(path, count, n_classes):
    """
    Read map data
    :param path: binary file path. \
        It can be presented as "flatten" array with next structure `float32 [count, n_classes + 2, 16, 16]`.
    :type path: str
    :param count: record count
    :type count: int
    :param n_classes: class count
    :type n_classes: int
    :return: map data as [count, 1, 16, 16]
    :rtype: numpy.ndarray
    """
    byte_count = count * (n_classes + 2) * 16 * 16 * 4
    with open(path, "rb") as source:
        map_bytes = source.read(byte_count)
    map_flat = numpy.fromstring(map_bytes, numpy.int32)
    map = map_flat.reshape([count, n_classes + 2, 16, 16])
    from_index = 2
    to_index = from_index + n_classes
    map_narrowed = map[:, from_index:to_index, :, :]
    if n_classes == 1:
        map_narrowed = map_narrowed.reshape([count, 1, 16, 16])
    return map_narrowed
