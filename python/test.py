import argparse
import loader
import numpy
from models import Depth, Map, Semantic


def main(args):
    if args["test"] == "rgb":
        rgb_test(args["rgb"], args["rgb_output"])
    elif args["test"] == "depth":
        depth_test(args["depth"], args["depth_output"])
    elif args["test"] == "semantic":
        semantic_test(args["semantic"], args["semantic_output"])
    elif args["test"] == "map":
        map_test(args["map"], args["map_output"])
    elif args["test"] == "weights":
        weight_test(args["weights"], args["weight"], args["weights_output"])
    elif args["test"] == "depth_cnn":
        depth_cnn_test(args["weights"], args["rgb"], args["cnn_output"])
    elif args["test"] == "map_cnn":
        map_cnn_test(args["weights"], args["rgb"], args["cnn_output"])
    elif args["test"] == "semantic_cnn":
        semantic_cnn_test(args["weights"], args["rgb"], args["cnn_output"])
    else:
        print("Invalid test")
        exit(1)


def rgb_test(source, target):
    print("Loading RGB data from", source)
    images = loader.read_rgb(source, 1000)
    numpy.save(target, images)
    print("Saved RGB data")


def depth_test(source, target):
    print("Loading depth data from", source)
    depth = loader.read_depth(source, 1000)
    numpy.save(target, depth)
    print("Saved depth data")


def semantic_test(source, target):
    print("Loading semantic data from", source)
    semantic = loader.read_semantic(source, 1000, 1)
    numpy.save(target, semantic)
    print("Saved depth data")


def map_test(source, target):
    print("Loading map data from", source)
    map = loader.read_map(source, 1000, 1)
    numpy.save(target, map)
    print("Saved map data")


def weight_test(source, weight, target):
    print("Loading weights data from", source)
    source_path = source + "/" + str(weight) + ".npy"
    numpy.save(target, numpy.load(source_path))
    print("Saved weight data")


def depth_cnn_test(weights_path, rgb, output):
    print("Loading weights")
    weights = []
    for i in range(0, 56):
        path = weights_path + "/" + str(i) + ".npy"
        weight = numpy.load(path)
        weights.append(weight)
    print("Loading images")
    images = loader.read_rgb(rgb, 8)
    print("Instantiating model")
    model = Depth(weights, 8)
    print("Calculation output")
    prediction = model.predict(images, True)
    numpy.save(output, prediction)
    print("Saved depth CNN output")


def map_cnn_test(weights_path, rgb, output):
    print("Loading weights")
    weights = []
    for i in range(0, 36):
        path = weights_path + "/" + str(i) + ".npy"
        weight = numpy.load(path)
        weights.append(weight)
    print("Loading images")
    images = loader.read_rgb(rgb, 8)
    print("Instantiating model")
    model = Map(4, 16, weights, 8)
    print("Calculation output")
    prediction = model.predict(images, True)
    numpy.save(output, prediction)
    print("Saved depth CNN output")


def semantic_cnn_test(weights_path, rgb, output):
    print("Loading weights")
    weights = []
    for i in range(0, 56):
        path = weights_path + "/" + str(i) + ".npy"
        weight = numpy.load(path)
        weights.append(weight)
    print("Loading images")
    images = loader.read_rgb(rgb, 8)
    print("Instantiating model")
    model = Semantic(4, weights, 8)
    print("Calculation output")
    prediction = model.predict(images, True)
    numpy.save(output, prediction)
    print("Saved semantic CNN output")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-test")
    parser.add_argument("--rgb")
    parser.add_argument("--rgb_output")
    parser.add_argument("--depth")
    parser.add_argument("--depth_output")
    parser.add_argument("--semantic")
    parser.add_argument("--semantic_output")
    parser.add_argument("--map")
    parser.add_argument("--map_output")
    parser.add_argument("--weights")
    parser.add_argument("--weight")
    parser.add_argument("--weights_output")
    parser.add_argument("--cnn_output")
    args = vars(parser.parse_args())
    main(args)