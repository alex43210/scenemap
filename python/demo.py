import argparse
from scipy.misc import imread, imresize
from models import Depth, Map, Semantic
import numpy


def main(args):
    image = imresize(imread(args["image"]), (224,224)).transpose([2, 0, 1]).reshape([1, 3, 224, 224])
    if args["model"] == "depth":
        model = initialize_depth_model(args["weights"])
    elif args["model"] == "map":
        model = initialize_map_model(args["weights"])
    elif args["model"] == "semantic":
        model = initialize_semantic_model(args["weights"])
    output = model.predict(image)
    numpy.save(args["output"], output)


def load_weights(path, count):
    result = []
    for i in range(0, count):
        result.append(numpy.load(path + "/" + str(i) + ".npy"))
    return result


def initialize_depth_model(weights_path):
    weights = load_weights(weights_path, 56)
    model = Depth(weights, 1)
    return model


def initialize_map_model(weights_path):
    weights = load_weights(weights_path, 36)
    model = Map(4, 16, weights, 1)
    return model


def initialize_semantic_model(weights_path):
    weights = load_weights(weights_path, 56)
    model = Semantic(4, weights, 1)
    return model


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-image")
    parser.add_argument("-weights")
    parser.add_argument("-model")
    parser.add_argument("-output")
    args = vars(parser.parse_args())
    main(args)