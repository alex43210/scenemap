#!/bin/bash

function test {
    #echo "$1"
    #cd lua
    #time $TORCH test.lua $2
    #if [ $? -ne 0 ]; then
    #    exit
    #fi
    #cd ..
    cd python
    echo $PYTHON test.py $3
    #time $PYTHON test.py $3
    if [ $? -ne 0 ]; then
        exit
    fi
    cd ..
    #$PYTHON compare.py $4
    #if [ $? -ne 0 ]; then
    #    exit
    #fi
}

function clearTestData {
    rm -rf test_data/*
}

function testRGB {
    test "RGB data reader test" \
        "-gpu -1 -test rgb -rgb $1 -rgb_output ../test_data/rgb_lua.t7" \
        "-test rgb --rgb $1 --rgb_output ../test_data/rgb_python.npy" \
        "test_data/rgb_lua.t7 test_data/rgb_python.npy 1e-3"
}

function testDepth {
    test "Depth data reader test" \
        "-gpu -1 -test depth -depth $1 -depth_output ../test_data/depth_lua.t7" \
        "-test depth --depth $1 --depth_output ../test_data/depth_python.npy" \
        "test_data/depth_lua.t7 test_data/depth_python.npy 1e-3"
}

function testSemantic {
    test "Semantic data reader test" \
        "-gpu -1 -test semantic -semantic $1 -semantic_output ../test_data/semantic_lua.t7" \
        "-test semantic --semantic $1 --semantic_output ../test_data/semantic_python.npy" \
        "test_data/semantic_lua.t7 test_data/semantic_python.npy 1e-3"
}

function testMap {
    test "Map data reader test" \
        "-gpu -1 -test map -map $1 -map_output ../test_data/map_lua.t7" \
        "-test map --map $1 --map_output ../test_data/map_python.npy" \
        "test_data/map_lua.t7 test_data/map_python.npy 1e-3"
}

function testWeightsDirectory {
    index=0
    while [ $index -lt $3 ]
    do
        test "${2} weight loader test $index" \
            "-gpu -1 -test weights -weights ../$1 -weight $index -weights_output ../test_data/${2}_weight_${index}_lua.t7" \
            "-test weights --weights ../$1 --weight $index --weights_output ../test_data/${2}_weight_${index}_python.npy" \
            "test_data/${2}_weight_${index}_lua.t7 test_data/${2}_weight_${index}_python.npy 1e-3"
        index=`expr $index + 1`
    done
}

function testWeights {
    testWeightsDirectory "$1/depth" "depth" 55
    testWeightsDirectory "$1/map" "map" 35
    testWeightsDirectory "$1/semantics" "semantic" 55
}

function testDepthCnn {
    weights="../$1"
    data="$2"
    #testRGB "$data"
    test "Depth CNN test" \
        "-gpu -1 -test depth_cnn -weights $weights -rgb $data -cnn_output ../test_data/depth_cnn_lua.t7" \
        "-test depth_cnn --weights $weights --rgb $data --cnn_output ../test_data/depth_cnn_python.npy" \
        "test_data/depth_cnn_lua.t7 test_data/depth_cnn_python.npy 1.5e-3"
}

function testMapCnn {
    weights="../$1"
    data="$2"
    #testRGB "$data"
    test "Map CNN test" \
        "-gpu -1 -test map_cnn -weights $weights -rgb $data -cnn_output ../test_data/map_cnn_lua.t7" \
        "-test map_cnn --weights $weights --rgb $data --cnn_output ../test_data/map_cnn_python.npy" \
        "test_data/map_cnn_lua.t7 test_data/map_cnn_python.npy 1e-3"
}

function testSemanticCnn {
    weights="../$1"
    data="$2"
    #testRGB "$data"
    test "Semantic CNN test" \
        "-gpu -1 -test semantic_cnn -weights $weights -rgb $data -cnn_output ../test_data/semantic_cnn_lua.t7" \
        "-test semantic_cnn --weights $weights --rgb $data --cnn_output ../test_data/semantic_cnn_python.npy" \
        "test_data/semantic_cnn_lua.t7 test_data/semantic_cnn_python.npy 1e-3"
}

if [ "$PYTHON" == "" ]; then
    PYTHON="python3"
fi
if [ "$TORCH" == "" ]; then
    TORCH="th"
fi
export THEANO_FLAGS="device=gpu0,mode=FAST_RUN"
if [ ! -d test_data ]; then
    mkdir test_data
fi
TEST="$1"
ARGUMENT="$2"
ARGUMENT2="$3"
if [ "$TEST" == "rgb_loader" ]; then
    testRGB "$ARGUMENT"
fi
if [ "$TEST" == "depth_loader" ]; then
    testDepth "$ARGUMENT"
fi
if [ "$TEST" == "semantic_loader" ]; then
    testSemantic "$ARGUMENT"
fi
if [ "$TEST" == "map_loader" ]; then
    testMap "$ARGUMENT"
fi
if [ "$TEST" == "weights_loader" ]; then
    testWeights "$ARGUMENT"
fi
if [ "$TEST" == "depth_cnn" ]; then
    testDepthCnn "$ARGUMENT" "$ARGUMENT2"
fi
if [ "$TEST" == "map_cnn" ]; then
    testMapCnn "$ARGUMENT" "$ARGUMENT2"
fi
if [ "$TEST" == "semantic_cnn" ]; then
    testSemanticCnn "$ARGUMENT" "$ARGUMENT2"
fi
clearTestData