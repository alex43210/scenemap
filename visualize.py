import sys
import torchfile
import numpy
from scipy.misc import imsave, imresize

m_values=[]

def depth_visualization(depth_data):
    image = numpy.array([depth_data, depth_data, depth_data]).reshape([3, 224, 224]).transpose([1, 2, 0])
    image -= image.min()
    image = image / image.max() * 255
    return image


def max_class_visualization(classes_data, colormap, size):
    image = classes_data - classes_data.min()
    image /= image.max()
    converted = numpy.zeros([size, size, 3])
    for row in range(0, size):
        for column in range(0, size):
            values = image[row, column]
            max_index = values.argmax()
            if values[max_index] >= 0.4:
                color = colormap[max_index]
                converted[row, column] = color
                converted[row, column] = color
    return converted


def map_visualization(map_data):
    return imresize(
        max_class_visualization(
            map_data,
            numpy.array([
                [100, 100, 100],
                [  0,   0, 255],
                [  0, 255,   0],
                [255,   0,   0]
            ]),
            16
        ),
        (224, 224)
    )


def semantic_visualization(semantic_data):
    return max_class_visualization(
        semantic_data,
        numpy.array([
            [  0, 255,  11],
            [251, 255,   0],
            [255,   0,  16],
            [255,   0, 225],
            [ 26,   0, 255],
            [  0, 211, 255],
        ]),
        224
    )


if __name__ == '__main__':
    assert len(sys.argv) == 5
    type = sys.argv[1]
    source = sys.argv[2]
    target = sys.argv[3]
    conversion = sys.argv[4]
    if type == "torch":
        data = torchfile.load(source)
    else:
        data = numpy.load(source)
    if conversion == "depth":
        image = depth_visualization(data.reshape([1, 224, 224]))
    elif conversion == "map":
        reshaped = data.reshape([4, 16, 16]).transpose([1, 2, 0])
        image = map_visualization(reshaped)
    elif conversion == "semantic":
        reshaped = data.reshape([6, 224, 224]).transpose([1, 2, 0])
        image = semantic_visualization(reshaped)
    imsave(target, image)