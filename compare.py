import sys
from sklearn.metrics import mean_squared_error
import torchfile
import numpy


if __name__ == "__main__":
    torch_data = torchfile.load(sys.argv[1])
    assert isinstance(torch_data, numpy.ndarray)
    print("Torch data shape", torch_data.shape)
    numpy_data = numpy.load(sys.argv[2])
    assert isinstance(numpy_data, numpy.ndarray)
    print("Python data shape", numpy_data.shape)
    max_error = float(sys.argv[3])
    assert numpy_data.shape == torch_data.shape
    print("Torch data", numpy.array(torch_data.flat), "min", torch_data.min(), "max", torch_data.max(), "mean", torch_data.mean())
    print("Python data", numpy.array(numpy_data.flat), "min", numpy_data.min(), "max", numpy_data.max(), "mean", numpy_data.mean())
    diff = numpy.absolute(numpy.array(numpy_data.flat) - numpy.array(torch_data.flat))
    print("Diff max", diff.max(), "min", diff.min(), "mean", diff.mean())
    error = mean_squared_error(numpy.array(numpy_data.flat), numpy.array(torch_data.flat))
    print("Error value is ", error)
    assert error <= max_error